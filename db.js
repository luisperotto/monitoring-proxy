import knex from "knex";
import { DATABASE_URL } from "./config.js";

let db = knex({
	client: "mysql2",
	connection: DATABASE_URL,
});

export default db;
