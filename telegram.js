import TelegramBot from "node-telegram-bot-api";
import { TOKEN } from "./config.js";

let bot = new TelegramBot(TOKEN, {
	polling: true,
});

async function sendMessage(chat_id, msg) {
	await bot.sendMessage(chat_id, msg);
}

export { bot, sendMessage };
