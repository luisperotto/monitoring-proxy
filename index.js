import { CHATID } from "./config.js";
import { bot, sendMessage } from "./telegram.js";
import knex from "./db.js";

bot.on("message", async (msg) => {
	let chat_id = parseInt(CHATID);
	if (msg.chat.id === chat_id) {
		let [data] = await knex.raw(`SELECT 
	ba.id,
	bank.name,
	ba.account_holder_name,
	ba.account_numbers,
	ba.scraper_status,
	(select count(DISTINCT(spba.id)) from bankgateway.scraper_proxies_bank_accounts as spba INNER JOIN bankgateway.bank_accounts as ba1 ON ba1.id = spba.bank_account_id WHERE spba.bank_account_id = ba.id AND ba.bank_id = '2' AND spba.is_blocked = '1') as blocked_proxies
  FROM bank_accounts as ba INNER JOIN bankgateway.banks as bank ON bank.id = ba.bank_id WHERE ba.bank_id = '2' AND ba.id NOT IN ('3','4','6')`);

		data.map(async (el) => {
			let text = `[${el.id}]\n${el.name} ${el.account_numbers} / ${el.account_holder_name}\n${el.scraper_status} - ${el.blocked_proxies}`;
			await sendMessage(chat_id, text);
		});
	}
	console.log("msg", msg.text);
});
