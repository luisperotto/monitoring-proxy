import dotenv from 'dotenv'
dotenv.config({ path: '.env' })

let DATABASE_URL = process.env.DATABASE_URL
let TOKEN = process.env.TOKEN
let CHATID = process.env.CHATID

export {
	DATABASE_URL,
	TOKEN,
	CHATID
}
